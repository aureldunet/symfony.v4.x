include .env

APP_SERVICE = app

DOCKER_COMPOSE = docker-compose
DOCKER_COMPOSE_EXEC = $(DOCKER_COMPOSE) exec
DOCKER_COMPOSE_RUN = $(DOCKER_COMPOSE) run

EXEC_APP = $(DOCKER_COMPOSE_EXEC) app
EXEC_APP_USER = $(DOCKER_COMPOSE_EXEC) -u $(APP_USER) app
EXEC_APP_COMPOSER = $(EXEC_APP_USER) composer
EXEC_APP_CONSOLE = $(EXEC_APP_USER) bin/console

permission-install:
	$(DOCKER_COMPOSE_RUN) $(APP_SERVICE) chmod 777 -R .

up:
	$(DOCKER_COMPOSE) -f $(APP_COMPOSE_FILE) up -d --build

down:
	$(DOCKER_COMPOSE) down -v

composer-install:
	$(EXEC_APP_COMPOSER) install --no-suggest --no-progress

db-drop:
	$(EXEC_APP_CONSOLE) doctrine:database:drop --force --if-exists

db-create:
	$(EXEC_APP_CONSOLE) doctrine:database:create --if-not-exists

db-migrate:
	$(EXEC_APP_CONSOLE) doctrine:migrations:migrate -n

db-diff:
	$(EXEC_APP_CONSOLE) doctrine:migrations:diff

db-fixtures:
	$(EXEC_APP_CONSOLE) doctrine:fixtures:load -n

db-install: db-drop db-create # db-migrate

install:  up composer-install db-install permission-install
