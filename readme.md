# Docker Symfony 4.x

![Symfony](public/sf.png)

v.4.3.4

## Requirements

- git
- docker
- docker-compose
- make

## Install

Change parameters into .env.dev

```
# In all environments, the following files are loaded if they exist,
# the later taking precedence over the former:
#
#  * .env                contains default values for the environment variables needed by the app
#  * .env.local          uncommitted file with local overrides
#  * .env.$APP_ENV       committed environment-specific defaults
#  * .env.$APP_ENV.local uncommitted environment-specific overrides
#
# Real environment variables win over .env files.
#
# DO NOT DEFINE PRODUCTION SECRETS IN THIS FILE NOR IN ANY OTHER COMMITTED FILES.
#
# Run "composer dump-env prod" to compile .env files for production use (requires symfony/flex >=1.2).
# https://symfony.com/doc/current/best_practices/configuration.html#infrastructure-related-configuration

###> symfony/framework-bundle ###
APP_ENV=dev
APP_SECRET=72837a9d6bf6a48aad59ca049ed16732
APP_COMPOSE_FILE=docker-compose.yml
APP_USER=www-data
APP_MODE=777
#TRUSTED_PROXIES=127.0.0.1,127.0.0.2
#TRUSTED_HOSTS='^localhost|example\.com$'
###< symfony/framework-bundle ###

###> doctrine/doctrine-bundle ###
# Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# For an SQLite database, use: "sqlite:///%kernel.project_dir%/var/data.db"
# Configure your db driver and server_version in config/packages/doctrine.yaml
DATABASE_URL=mysql://root:password@mysql:3306/db_name_%kernel.environment%
###< doctrine/doctrine-bundle ###

###> symfony/swiftmailer-bundle ###
# For Gmail as a transport, use: "gmail://username:password@localhost"
# For a generic SMTP server, use: "smtp://localhost:25?encryption=&auth_mode="
# Delivery is disabled by default via "null://localhost"
MAILER_URL=null://mailhog:1025
###< symfony/swiftmailer-bundle ###
```

Run

```
git config core.fileMode false
```

Run 

```
make install
```

## Usage

```
make install
make up
make down
make composer-install
make db-drop
make db-create
make db-diff
make db-migrate
make db-fixtures
make db-install
```

## Access

| Service        | Url                     | Login        | Password      |
| -------------- | ----------------------- | ------------ | ------------- |
| Front          | http://localhost/       | na           | na            |
| PhpMyAdmin     | http://localhost:8081/  | root         | password      |
| MailHog        | http://localhost:8025/  | na           | na            |
